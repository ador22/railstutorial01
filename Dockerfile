FROM ruby:2.3.0-onbuild 
MAINTAINER adrienn.szabo4@gmail.com

## Based on: https://blog.codeship.com/running-rails-development-environment-docker/

# Install apt based dependencies required to run Rails as 
# well as RubyGems

RUN apt-get update && apt-get install -y \ 
  build-essential \ 
  nodejs

# Configure the main working directory. This is the base 
# directory used in any further RUN, COPY, and ENTRYPOINT 
# commands.
RUN mkdir -p /app 
WORKDIR /app

# TODO
#COPY Gemfile Gemfile.lock ./ 
#RUN gem install bundler && bundle install --jobs 20 --retry 5

# Copy the main application.
#COPY . ./

# Expose port 3000 to the Docker host, so we can access it 
# from the outside.
#EXPOSE 3000

# The main command to run when the container starts
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]

